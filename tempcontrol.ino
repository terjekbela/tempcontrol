////////////////////////////////////////////////////////////////////////////////
// TEMPControl
////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
// Libraries used
////////////////////////////////////////////////////////////////////////////////

#define XCONTROL_NAME F("TEMPControl")      // application name
#define XCONTROL_VER  F("v1.0.5")           // application version

#include <Wire.h>                           // Arduino standard I2C library

//#include <SPI.h>                          // Arduino standard Ethernet/SPI lib
//#include <Ethernet.h>

#include <UIPEthernet.h>                    // UIP Ethernet library
#undef UIPETHERNET_DEBUG_CLIENT             // https://github.com/ntruchsess/arduino_uip

#include <SHT2x.h>                          // Temp and Humidity sensor library
#define SHT2x_DEBUG  == 0                   // https://github.com/misenso/SHT2x-Arduino-Library

#include <Adafruit_BMP085.h>                // Pressure sensor library
#define BMP085_DEBUG == 0                   // https://github.com/claws/BH1750

#include <BH1750.h>                         // light sensor library
#define BH1750_DEBUG == 0                   // https://github.com/claws/BH1750



////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// menu configuration
const byte menuCount PROGMEM     = 2;      // number of menu items
const String menuNames[]         = {       // menu display naming
  "menu 1",
  "menu 2"
};
const String menuLinks[]         = {       // menu href links
  "http://192.168.xxx.xxx",
  "http://192.168.xxx.xxx"
};

// network configuration
byte netMAC[]                    = {       // mac address
  0xFE, 0xE5, 0x93,
  0x89, 0xF6, 0x00
};
byte netIP[]                     = {       // ip address
  192, 168, 15, 16
};



////////////////////////////////////////////////////////////////////////////////
// Runtime variables
////////////////////////////////////////////////////////////////////////////////

//ethernet module setup
EthernetServer netServer(80);

// pressure sensor setup
Adafruit_BMP085 bmp;

// light meter setup
BH1750 lightMeter;

// sensor values and update timestamps
float         sensorTempValue;
unsigned long sensorTempUpdate     = 0;
float         sensorHumidityValue;
unsigned long sensorHumidityUpdate = 0;
int32_t       sensorPressureValue;
unsigned long sensorPressureUpdate = 0;
uint16_t      sensorLightValue;
unsigned long sensorLightUpdate    = 0;



////////////////////////////////////////////////////////////////////////////////
// Setup / main section
////////////////////////////////////////////////////////////////////////////////

// setup routine
void setup() {
  Wire.begin();                            // i2c protocol init
  lightMeter.begin();                      // light sensor
  bmp.begin();                             // pressure sensor
  Serial.begin(9600);                      // serial
  Ethernet.begin(netMAC, netIP);           // ethernet
  netServer.begin();
}

// main loop
void loop() {
  interruptSerial();
  interruptHttp();
  interruptSensor();
  delay(100);
}



////////////////////////////////////////////////////////////////////////////////
// HTTP and Serial request processing
////////////////////////////////////////////////////////////////////////////////

// main serial processing
void serialProcess() {
  // ...
}

// main http processing
void httpProcess(EthernetClient ec, String url) {
  if(url.startsWith("/temp") or url=="" or url=="/") {
    htmlHeader(ec);
    htmlMenu(ec);
    htmlTempCard(ec);
    htmlHumidityCard(ec);
    htmlPressureCard(ec);
    htmlLightCard(ec);
    htmlFooter(ec);
  } 
  else if(url.startsWith("/css")) {
    htmlCSS(ec);
  } 
  else if(url.startsWith("/js")) {
    htmlJS(ec);
  } 
  else if(url.startsWith("/svg")) {
    url.replace("/svg", "");
    if(url.startsWith("/cog")) {

    }
  }
}

//
void htmlTempCard(EthernetClient ec) {
  ec.println(F("  <card>"));
  ec.print  (F("    <name>Temperature: "));    
  ec.print  (sensorTempValue);    
  ec.println(F("C</name>"));    
  ec.println(F("    <desc>sdafsdf sadfsa dfsadfsdafsa dfsadfsadfsa fdsadfs dfsadfsa dfsadf</desc>"));    
  ec.println(F("  </card>"));
}

//
void htmlHumidityCard(EthernetClient ec) {
  ec.println(F("  <card>"));
  ec.print  (F("    <name>Humidity: "));    
  ec.print  (sensorHumidityValue);    
  ec.println(F("%</name>"));    
  ec.println(F("    <desc>sdafsdf sadfsa dfsadfsdafsa dfsadfsadfsa fdsadfs </desc>"));    
  ec.println(F("  </card>"));
}

//
void htmlPressureCard(EthernetClient ec) {
  ec.println(F("  <card>"));
  ec.print  (F("    <name>Pressure: "));    
  ec.print  (sensorPressureValue);    
  ec.println(F("hPa</name>"));    
  ec.println(F("    <desc>sdafsdf sadfsa dfsadfsdafsa dfsadfsadfsa fdsadfs dfsadfsa dfsadsadfsad f</desc>"));    
  ec.println(F("  </card>"));
}

//
void htmlLightCard(EthernetClient ec) {
  ec.println(F("  <card>"));
  ec.print  (F("    <name>Light: "));    
  ec.print  (sensorLightValue);    
  ec.println(F("lux</name>"));    
  ec.println(F("    <desc>sdafsdf sadfsa dfsadfsdafsa dfsadfsadfsa fdsadfs dfsadfsa dffsad f</desc>"));    
  ec.println(F("  </card>"));
}

//
void interruptSensor() {
  if(sensorTempUpdate == 0 or millis() - (sensorTempUpdate) > 1000 ) {
    sensorTempValue      = SHT2x.GetTemperature();
    sensorTempUpdate     = millis();
  }
  if(sensorHumidityUpdate == 0 or millis() - (sensorHumidityUpdate) > 1000 ) {
    sensorHumidityValue  = SHT2x.GetHumidity();
    sensorHumidityUpdate = millis();
  }
  if(sensorPressureUpdate == 0 or millis() - (sensorPressureUpdate) > 1000 ) {
    sensorPressureValue  = bmp.readSealevelPressure(180) / 100;
    sensorPressureUpdate = millis();
  }
  if(sensorLightUpdate == 0 or millis() - (sensorLightUpdate) > 1000 ) {
    sensorLightValue     = lightMeter.readLightLevel();
    sensorLightUpdate    = millis();
  }
}



////////////////////////////////////////////////////////////////////////////////
// Common HTTP, HTML and serial functions for RELAY/LED/TEMP Control sketches
////////////////////////////////////////////////////////////////////////////////

// handle basic request received via serial port
void interruptSerial() {
  // ...
}

// handle basic request received with http
void interruptHttp() {
  boolean currentBlank;
  String currentLine;
  String currentURL;
  String currentReq;
  char c;
  EthernetClient client = netServer.available();
  if (client) {
    currentBlank = true;
    currentLine  = "";
    currentURL   = "";
    //    currentReq   = "";
    while (client.connected()) {
      if (client.available()) {
        c = client.read();
        //        currentReq.concat(c);
        if (c == '\n' && currentBlank) {
          //          httpProcess(client, currentURL, currentReq);
          httpProcess(client, currentURL);
          break;
        }
        if (c=='\n' or c=='\r') {
          currentBlank = true;
          if(currentLine.indexOf('GET') > 0) {
            currentURL = currentLine;
            currentURL.replace("GET", "");
            currentURL.replace("HTTP/1.1", "");
            currentURL.replace(" ", "");
            currentLine = "";
          }
        } 
        else if (c != '\r') {
          currentBlank = false;
          currentLine.concat(c);
        }
      }
    }
    delay(50);
    client.stop();
  }
}

// http response header
void httpHeader(EthernetClient ec, String ct) {
  ec.println(F("HTTP/1.1 200 OK"));
  ec.print  (F("Content-Type: "));
  ec.println(ct);  
  ec.println(F("Connection: close"));
  ec.println();
}

// html header fields and start html body
void htmlHeader(EthernetClient ec) {
  httpHeader(ec, "text/html");
  ec.println(F("<!DOCTYPE html>"));
  ec.println(F("<html>"));
  ec.println(F("<head>"));
  ec.println(F("<meta name=\"viewport\" content=\"width=500, initial-scale=1\">"));
  ec.println(F("<meta charset=\"utf-8\"/>"));
  ec.println(F("<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/xcontrol.css\">"));
  ec.println(F("<link rel=\"stylesheet\" type=\"text/css\" href=\"//fonts.googleapis.com/css?family=Roboto&amp;subset=latin,latin-ext\">"));
  ec.println(F("<title>"));
  ec.println(XCONTROL_NAME);
  ec.println(F("</title>"));
  ec.println(F("</head>"));
  ec.println(F("<body>"));
}

// html footer and ending
void htmlFooter(EthernetClient ec){
  ec.println(F("</body>"));
  ec.println(F("</html>"));
}

// html menu 
void htmlMenu(EthernetClient ec) {
  ec.println(F("  <menu>"));
  for(byte i=0; i<menuCount; i++){
    ec.print(F("    <a href=\"")); 
    ec.print(menuLinks[i]); 
    ec.print(F("\">")); 
    ec.print(menuNames[i]); 
    ec.println(F("</a>"));
  }
  ec.println(F("  </menu>"));
  ec.println(F("  <submenu>"));
  ec.println(F("  <a href=\"#\">Living room temp</a>"));
  ec.println(F("  <a href=\"#\">Switches</a>"));
  ec.println(F("  <a href=\"#\">LED stairs</a>"));
  ec.println(F("  <a class=\"refresh\" href=\"/\">X</a>"));
  ec.println(F("  </submenu>"));
}

// css file
void htmlCSS(EthernetClient ec) {
  httpHeader(ec, "text/css");
  ec.println(F("* {font-family:Roboto,Helvetica,Arial,sans-serif;font-weight:normal;margin:0;padding:0;}"));
  ec.println(F("body {background-color:#eee;}"));
  ec.println(F("menu {width:100%;height:80px;background-color:#3f51b5;text-align:center;}"));
  ec.println(F("menu a {display:inline-block;color:white;line-height:40px;text-decoration:none;margin:0 20px;}"));
  ec.println(F("menu a:hover {border-bottom:solid 3px white;line-height:34px;}"));
  ec.println(F("submenu {padding:0 20px;height:80px;background-color:#9fa8da;margin-top:-40px !important;margin-bottom:30px !important;}"));
  ec.println(F("submenu a {display:inline-block;height:40px;line-height:40px;color:white;text-decoration:none;margin-right:20px;}"));
  ec.println(F("submenu a.refresh {float:right;color:white;background-color:#3f51b5;width:40px;height:40px;line-height:40px;margin-top:59px;margin-right:0;text-align:center;border-radius:50%;}"));
  ec.println(F("card,submenu {display:block;width:100%;max-width:500px;margin:10px auto;font-size:1.2em;border-radius:2px;box-shadow:0 10px 36px 0 rgba(0,0,0,0.2);}"));
  ec.println(F("card {padding:20px;background-color:white;color:#3f51b5;}"));
  ec.println(F("name {display:inline-block;width:100%;}"));
  ec.println(F("desc {display:inline-block;width:100%;font-size:0.8em;color:black;margin-top:10px;}"));
  ec.println(F("action {display:block;width:100%;height:36px;margin-top:20px;font-size:0.8em;line-height:40px;color:#c0c0c0;cursor:pointer;}"));
  ec.println(F("state,select {display:inline-block;font-size:0.8em;height:36px;line-height:36px;margin-left:20px;float:right;border-radius:2px;}"));
  ec.println(F("state {padding:0 20px;text-align:center;color:#9fa8da;background-color:white;transition:box-shadow 0.3s cubic-bezier(0.4,0,0.2,1)}"));
  ec.println(F("state:hover {box-shadow: 0px 4px 8px 0px rgba(0,0,0,0.4);}"));
  ec.println(F("state.active {color:white;background-color:#9fa8da;}"));
  ec.println(F("select {border:none;background-color:#9fa8da;color:white;appearance:none;-webkit-appearance:none;-moz-appearance:none;padding:0 15px 0 10px;}"));
  ec.println(F("option {height:36px;line-height:36px;}"));
}

// js file
void htmlJS(EthernetClient ec) { 
  httpHeader(ec, "application/javascript");
  ec.println(F("sdfdsfdsf"));  
}

