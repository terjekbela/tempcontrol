TEMPControl
...is an open source appliance firmware for the Arduino platform. It can be used for home automation and industrial control applications. It is part of the XControl family.

Usage
  You can use TempControl from a browser manually or invoke it from another system using HTTP API.

  The HTTP API uses the URL to encode request data elements (such as the requested sensor name) and returns the information in formats using plain text, json and xml. As TEMPControl is a sensor app, there is nothing too much to be set at the sensor, so this API is read-only. (other members of the XControl family do have write functionality)

  The human interface is using a Google's Material Design principles and maintains a simple format. The interface is written in a way to be very space efficient. It has to fit into Arduino's program space along with the actual control functionality and the rather large libraries used for network and sensor communication.

HTTP API
  http://1.2.3.4/temp/json/1
    returns: '{"temp":"23","unit":"C","status":true,"message":"ok"}'

  http://1.2.3.4/pressure/xml/1
    returns: '<?xml?><tempcontrol><temp>23</temp><unit>C</unit><status>true</status><message>ok</message></tempcontrol>'

  http://1.2.3.4/humidity/plain/1
    returns: '54'

  http://1.2.3.4/light/json/1
    returns: '{"light":"234","unit":"lux","status":true,"message":"ok"}'

  http://1.2.3.4/ktypetemp/plain/1
    returns: '345'
  
Hardware supported and tested
  Microcontroller
    Arduino Leonardo/UNO/Duemilanove/Nano/Micro/ProMicro (and compatibles)
    ESP8266 ESP-03 (probably runs on other ESP-xx versions, too)
    NodeMCU Lua development board
  Network module
    Arduino WiFi shield on top of an Arduino
    Arduino Ethernet shield on top of an Arduino
    ENC28J60 module; either the normal or the shield version, connected to a Arduino Nano typically
    ESP8266 module connected to Arduino (not too practical, as TEMPControl runs directly on ESP chip)

Licensing
  Creatice Commons Share-Alike license
